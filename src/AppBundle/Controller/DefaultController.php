<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Letter;
use AppBundle\Form\LetterType;
use AppBundle\Service\IcndbAPIService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Swift_Mailer;
use Swift_Message;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    const PATH_TO_LETTERS = '/../web/files/letters/';

    /**
     * @Route("/", name="homepage")
     *
     * @return Response
     */
    public function indexAction()
    {
        $form = $this->buildForm();

        return $this->render('default/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/submit", name="submit")
     *
     * @param Request         $request
     * @param Swift_Mailer    $mailer
     * @param IcndbAPIService $service
     *
     * @return RedirectResponse
     */
    public function submitAction(Request $request, Swift_Mailer $mailer, IcndbAPIService $service)
    {
        $form = $this->buildForm();

        $form->handleRequest($request);

        if (!$form->isSubmitted() || !$form->isValid())
        {
            $this->setMessage($request, 'warning', 'Form is invalid');

            return $this->redirectToRoute('homepage');
        }

        $letter = $form->getData();

        $category = $letter->getCategory();
        $email = $letter->getEmail();

        $joke = $service->getJokeByCategory($category);

        $message = (new Swift_Message("Случайная шутка из $category"))
            ->setFrom('send@example.com')
            ->setTo($email)
            ->setBody(
                $this->renderView(
                    'emails/joke.html.twig',
                    ['joke' => $joke]
                ),
                'text/html'
            );

        $mailer->send($message);

        $this->writeJoke($joke);

        $this->setMessage($request, 'notice', 'Joke was sent');

        return $this->redirectToRoute('homepage');
    }

    /**
     * @return FormInterface
     */
    private function buildForm(): FormInterface
    {
        $letter = new Letter();

        return $this->createForm(LetterType::class, $letter, [
            'action' => $this->generateUrl('submit')
        ]);
    }

    /**
     * @param Request $request
     * @param string  $type
     * @param string  $message
     */
    private function setMessage(Request $request, string $type, string $message)
    {
        $session = $request->getSession();
        $session->start();
        $session->getFlashBag()->add($type, $message);
    }

    /**
     * @param string $joke
     */
    private function writeJoke(string $joke)
    {
        $fileSystem = new Filesystem();
        if (!$fileSystem->exists($this->get('kernel')->getRootDir() . self::PATH_TO_LETTERS))
        {
            $fileSystem->mkdir($this->get('kernel')->getRootDir() . self::PATH_TO_LETTERS);
        }

        file_put_contents(
            $this->get('kernel')->getRootDir() . self::PATH_TO_LETTERS . time() . '.txt',
            $joke
        );
    }
}
