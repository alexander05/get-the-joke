<?php

namespace AppBundle\Service\Manager;

use AppBundle\Service\IcndbAPIService;
use GuzzleHttp\Client;
use Symfony\Component\Cache\Simple\FilesystemCache;

class IcndbAPIManager implements IcndbAPIService
{
    const HOST = 'http://api.icndb.com/';

    /**
     * @return array
     */
    public function getCategories(): array
    {
        $cache = new FilesystemCache();

        $categories = $cache->get('icndb.categories');

        if (!$categories)
        {
            $client = new Client();
            $response = $client->get(self::HOST.'categories');

            $content = $response->getBody()->getContents();
            $result = json_decode($content);

            $categories = $result->value;

            $cache->set('icndb.categories', $categories, 60);
        }

        return array_combine($categories, $categories);
    }

    /**
     * @param string $category
     *
     * @return string
     */
    public function getJokeByCategory(string $category): string
    {
        $client = new Client();
        $response = $client->get(self::HOST."jokes/random?limitTo=[$category]");

        $content = $response->getBody()->getContents();
        $result = json_decode($content);

        return $result->value->joke;
    }
}