<?php

namespace Tests\AppBundle\Service\Manager;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class IcndbAPIManagerTest extends KernelTestCase
{
    private $service;

    public function setUp()
    {
        $kernel = self::bootKernel();

        $this->service = $kernel
            ->getContainer()
            ->get('AppBundle\Service\Manager\IcndbAPIManager');
    }

    public function testCategoriesAreNotEmpty()
    {
        $categories = $this->service->getCategories();
        $this->assertEquals((bool) $categories, true);
    }

    public function testJokeByCategoryNotEmpty()
    {
        $categories = $this->service->getCategories();

        $joke = $this->service->getJokeByCategory(array_shift($categories));

        $this->assertEquals((bool) $joke, true);
    }
}
