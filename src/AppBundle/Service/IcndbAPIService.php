<?php

namespace AppBundle\Service;

interface IcndbAPIService
{
    public function getCategories(): array;

    public function getJokeByCategory(string $category): string;
}